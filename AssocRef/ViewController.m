//
//  ViewController.m
//  AssocRef
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ViewController.h"
#import <objc/runtime.h>
@interface ViewController ()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *buttonLabel;

@end

@implementation ViewController
static const char kRepresentedObject;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)doSomeThing:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    objc_setAssociatedObject(alert, &kRepresentedObject, sender, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIButton * button = objc_getAssociatedObject(alertView, &kRepresentedObject);
    self.buttonLabel.text = [button titleLabel].text;
}

@end
